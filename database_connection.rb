# database connection class

require 'mongo'

class DatabaseConnection  
  
  def initialize()
    @@connection = Mongo::Connection.new.db("blog")
    @@collection = @@connection.collection("posts")
  end
  
  # need to escape post.
  def add_post_to_collection(post)
    @@collection.insert(post)
  end
  
  def add_test_posts()
    post1 = {"title" => "First Post!", "author" => "Henri", "Date" => Time.new}
    add_post_to_collection(post1)
  end
  
  def list_posts()
    @@collection.find.each { |row| puts row }
  end
  
  def remove_all_posts!()
    @@collection.remove()
  end
  
  def count_posts()
    @@collection.count
  end
  
end