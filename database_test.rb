# tests the mongoDB database connection

load 'database_connection.rb'

db = DatabaseConnection.new

puts "removing all posts!"
db.remove_all_posts!
puts "Count posts: "
puts db.count_posts

puts "Adding the test posts!"
puts "Count posts: " 
puts db.add_test_posts

puts "---"
puts "list of items in collection:"
puts db.list_posts
